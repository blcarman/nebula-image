FROM ubuntu:22.04

RUN apt update && apt-get upgrade -y && apt-get install -y wget

RUN wget https://github.com/slackhq/nebula/releases/download/v1.7.2/nebula-linux-amd64.tar.gz && \
    tar xvzf nebula-linux-amd64.tar.gz -C /usr/local/bin && \
    rm -f nebula-linux-amd64.tar.gz && \
    rm -f /usr/local/bin/nebula-cert

ENTRYPOINT ["/usr/local/bin/nebula"] 
CMD ["-version"]
